import React from 'react'
import './Button.css'


export default function Button({ content, type, onButtonClick }) {
  return (
    <>
      <div className={`button ${content === '0' ? 'zero' : ''} ${type || ''}`} onClick={onButtonClick(content)}>{content}</div>
    </>
  )
}
